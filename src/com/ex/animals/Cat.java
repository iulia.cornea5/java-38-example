package com.ex.animals;

import com.ex.animals.enums.Species;

import java.util.Objects;

public class Cat extends Animal implements Comparable {

    private String name;
    private int id;

    public Cat(String name, int id) {
        super(Species.CAT);
        this.name = name;
        this.id = id;
        totalCount++;
        animalsNames.add(this.name);
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String makeSound() {
        return "Miau";
    }

//
//    public boolean equals(Object obj) {
//        return (this == obj);
//    }

    @Override
    public boolean equals(Object o) {
        // daca e exact acelasi obiect (are aceeasi zona de memorie)
        if (this == o) return true;

        // daca al doilea obiect e null, nu are cum sa fie egale (this sigur nu e null)
        if (o == null) return false;

        // daca parametrul e de alt tip
        if (this.getClass() != o.getClass()) return false;

        // inseamna ca o e de fapt aceeasi clasa ca și this
        // si facem casting
        Cat catFromO = (Cat) o;
        if (this.id == catFromO.id) {
            return true;
        } else {
            return false;
        }
//        return id == catFromO.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


    // Cat arabella = new Cat("arabella", 7);
    // Cat other = new Cat("alta pisica", 9);
    // arabella.compareTo(other)
    //
    // this             arabella
    // o = cat          other


    @Override
    public int compareTo(Object o) {

        if (this.getClass() != o.getClass()) {
            return -1;
        } else {
            Cat cat = (Cat) o;
            if (this.id > cat.id) {
                return 1;
            } else if (this.id < cat.id) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
