package com.ex.animals;

import com.ex.animals.annotations.RiskLevel;
import com.ex.animals.enums.Species;

@RiskLevel(riskDegree = "LOW")
public class Cow extends Animal implements Comparable<Animal> {


    private String name;

    public Cow(String name) {
        super(Species.COW);
        this.name = name;
        totalCount++;
        animalsNames.add(this.name);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String makeSound() {
        return "Moo";
    }


    // "ana".compareTo("banana")    ->      -1
    // "ana".compareTo("ana")       ->      0
    // "ana".compareTo("alina")     ->      1

    @Override
    public int compareTo(Animal o) {
        if (this.getClass() != o.getClass()) {
            return -1;
        }
        Cow c = (Cow) o;
        if (this.getName().equals(c.getName())) {
            return 0;
        } else if (this.getName().compareTo(c.getName()) > 0) {
            return 1;
        } else {
            return -1;
        }
    }
}
