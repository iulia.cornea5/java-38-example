package com.ex.animals;

import java.sql.SQLOutput;
import java.util.Objects;

/**
 * Animal
 * - makeSound
 *
 * Dog, Cat, Sheep, Cow
 *
 */


public class MainAnimals {
    public static void main(String[] args) {
        Animal[] animals = new Animal[10];

        System.out.println("We have a total of " + Animal.totalCount + " animals");
        animals[0] = new Dog("Goofy", 12);
        animals[1] = new Cow("Milka");
        animals[2] = new Cat("Tommy", 1);
        animals[3] = new Dog("Pluto", 11);
        animals[4] = new Dog("Pluto", 13);
        animals[5] = new Cat("Mr. Belioz", 2);
        animals[6] = new Cat("Mr. Belioz", 2);

        System.out.println("We have a total of " + Animal.totalCount + " animals");


        for(int i=0; i<= 6; i++) {
            System.out.print(animals[i].getSpecies() + " " + animals[i].getName() + " ");
            System.out.println(animals[i].hashCode());
        }

        // equals <- metodată, actioneaza dupa cum e definită (by deafult e defintă la fel ca și "=="

        // == <- operator, verifica adresele

        if(animals[5] == animals[6]) {
            System.out.println("Mr. Belioz cat == Mr. Belioz cat true");
        } else {
            System.out.println("Mr. Belioz cat == Mr. Belioz cat false");
        }

        //       this.equals(Object o)
        if(animals[5].equals(animals[6])) {
            System.out.println("Mr. Belioz cat equals Mr. Belioz cat true");
        } else {
            System.out.println("Mr. Belioz cat equals Mr. Belioz cat false");
        }


        Cat tommyTheCat = new Cat("Tommy", 142342);
        Dog tommyTheDog = new Dog("Tommy", 142342);
        Object tommyTheCatObject = new Cat("Tommy", 142342);

        if(tommyTheCat.equals(tommyTheDog)) {
            System.out.println("tommyTheCat equals tommyTheDog");
        } else {
            System.out.println("tommyTheCat DOES NOT equal tommyTheDog");
        }

        if(tommyTheCat.equals(tommyTheCatObject)) {
            System.out.println("tommyTheCat equals tommyTheCatObject");
        }

    }

}
