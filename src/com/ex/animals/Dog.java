package com.ex.animals;
/*
        o andnotare noua new Annotation
        @Complicated
        @Complicate(value="VERY COMPLICATED")
 */


import com.ex.animals.annotations.Complicated;
import com.ex.animals.annotations.RiskLevel;
import com.ex.animals.annotations.VeryImportantClass;
import com.ex.animals.enums.Species;

import java.util.Objects;

@Complicated(value= "VERY COMPLICATED")
@RiskLevel(riskDegree = "HIGH")
@VeryImportantClass
public class Dog extends Animal implements Comparable {

    @Complicated(value= "VERY COMPLICATED")
    private String name;
    private int id;

    public Dog(String name, int id) {
        super(Species.DOG);
        this.name = name;
        this.id = id;
        totalCount++;
        animalsNames.add(this.name);
    }


    @RiskLevel(riskDegree = "HIGH")
    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }


    @Override
    public String makeSound() {
        return "Woof";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return id == dog.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Object o) {
        // Verificam ca "o" are aceeasi clasa ca si Dog.
        if (this.getClass() != o.getClass()) {
            return 1;
        } else {
            Dog dog = (Dog) o;
            if (this.id > dog.id) {
                return 1;
            } else if (this.id < dog.id) {
                return -1;
            } else {
                return 0;
            }
        }

    }
}
