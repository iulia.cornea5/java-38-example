package com.ex.animals;

import com.ex.animals.enums.Species;
import com.ex.food.Eatable;
import com.ex.food.Poisonous;

public class Chicken extends Animal implements Eatable, Poisonous {

    private String name;

    Chicken(String name) {
        // prima linie din constructor = apelarea construcorului clase parinte
        // super(....)
        super(Species.CHICKEN);
        this.name = name;
        totalCount++;
        animalsNames.add(this.name);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getKcal(){

        return 200;
    }

    @Override
    public int getProteinCount(){

        return 40;
    }

    @Override
    public int getCarbsCount(){

        return 2;
    }

    @Override
    public int getFatsCount(){

        return 12;
    }

    @Override
    public int getMaximumDosePerKg(){

        return -1;
    }

    @Override
    public String makeSound(){

        return "cotcodac";
    }

    @Override
    public float getDosePerKg() {
        return 0;
    }

    @Override
    public float getDoseForAdult(int kgAdult) {
        return 0;
    }

    public String getRecipe() {
        return "";
    }
}
