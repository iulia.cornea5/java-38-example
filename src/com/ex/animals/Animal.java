package com.ex.animals;

import com.ex.animals.annotations.VeryImportantClass;
import com.ex.animals.enums.Species;

import java.util.ArrayList;
import java.util.List;

@VeryImportantClass
public abstract class Animal {

    private Species species;

    Animal(Species species) {
        this.species = species;
    }

    public static int totalCount = 0;
    public static List<String> animalsNames = new ArrayList<>();

    abstract public String makeSound();
    abstract public String getName();

    @Deprecated
    public Species getSpecies() {
        return species;
    }
}
