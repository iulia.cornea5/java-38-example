package com.ex.claseanonime;

import com.ex.recapitulare.Cerc;
import com.ex.recapitulare.Forma;

import java.util.*;

public class MainStreamuri {

    public static void main(String[] args) {

        // Exemplu 1

        // Varianta 1 cu Clasă care implementează interfată (varianta clasică)
        CeaMaiMicaComparableClass celMaiMicObiect = new CeaMaiMicaComparableClass();
        // Varianta 2 cu clasă anonimă care are implementarea "in line"
        Comparable celMaiMicObiectComparabil = new Comparable() {
            @Override
            public int compareTo(Object o) {
                return -1;
            }
        };


        // Exemplu 2
        List<Integer> ints = new LinkedList<>();
        Map<Integer, String> intsMap = new TreeMap<>();

        Set<Forma> forme = new TreeSet<>();

        Cerc cerc = new Cerc(3);
        forme.add(cerc);
        Forma punct = new Forma() {
            @Override
            public double getArea() {
                return 1;
            }
        };
        forme.add(punct);


        forme.add(new Cerc(100));
        // add an entire plane
        forme.add(new Forma() {
            @Override
            public double getArea() {
                return Double.MAX_VALUE;
            }
        });
    }
}
