package com.ex.claseanonime;


// not a functional interface
public interface Action {

    void doAction();

    void doAction(int x);
}
