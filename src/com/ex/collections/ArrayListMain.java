package com.ex.collections;

import com.ex.communications.Sender;
import com.ex.communications.impl.ConsoleSender;
import com.ex.communications.impl.FileSender;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ArrayListMain {

    public static void main(String[] args) {
        String[] strings = new String[7];
        strings[0] = "zero";
        strings[1] = "unu";
        strings[2] = "trei";
        strings[3] = "patru";
        strings[4] = "cinci";
        strings[5] = "sase";

        for (int i = strings.length - 1; i > 2; i--) {
            strings[i] = strings[i - 1];
        }

        strings[2] = "doi";
        System.out.println("String[2] = " + strings[2]);

//        java.lang.ArrayIndexOutOfBoundsException: Index 2 out of bounds for length 2
//        strings[2] ="doi";
        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);
        }

//        interfata trebuie instantiata cu o clasa
//        Sender s = new ConsoleSender();


        List<String> stringArrayList = new ArrayList<String>();
        System.out.println("stringArrayList size = " + stringArrayList.size());
        stringArrayList.add("Primul");
        stringArrayList.add("Al doilea");
        stringArrayList.add("Al patrulea");
        System.out.println("stringArrayList size = " + stringArrayList.size());
        stringArrayList.add(2, "Al treilea");

        for (int i = 0; i < stringArrayList.size(); i++) {
            System.out.println(stringArrayList.get(i));
        }



        // read from keyboard a list of 10 strings
        // display if it contains "portocala"

        System.out.println("First exercise: We shall read 10 inputs from the console and then display them");
        List<String> listOf10 = new ArrayList<>();
        Scanner keyboard = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            System.out.println("Write a string and then press enter: ");
            String inputFromUser = keyboard.nextLine();
            listOf10.add(inputFromUser);
        }
        for (int i = 0; i < listOf10.size(); i++) {
            System.out.println("Element with index " + i + " is: " + listOf10.get(i));
        }



        // read from keyboard a list of strings until the user inputs the string "THE END"
        // display if it contains "portocala"
        System.out.println("Insert the elements");

        List<String> userInputList = new ArrayList<>();

        String userInput = keyboard.nextLine();
        while (!userInput.equals( "THE END")){
            userInputList.add(userInput);
            userInput = keyboard.nextLine();
        }

        boolean containsPortocala = false;
        // ala bala portocala ALA
        for (int i=0; i< userInputList.size();i++){
            if(userInputList.get(i).equals("portocala")) {
                containsPortocala = true;
            }
            System.out.println(userInputList.get(i));
        }

        System.out.println("Portocala e acolo? " + containsPortocala);
        System.out.println("Portocala e acolo? " + userInputList.contains("portocala"));


        // read a list of 7 integers
        // display the sum of all integers read
        List<Integer> listOfIntegers = new ArrayList<>();
        int sum = 0;
        for(int i = 0; i<7;i++) {
            System.out.println("listofIntegers[" + i + "]= ");
            int input = keyboard.nextInt();
            listOfIntegers.add(i,input );
            sum+=input;
        }


        System.out.println("The sum of the Integers list is: " + sum);

    }
}
