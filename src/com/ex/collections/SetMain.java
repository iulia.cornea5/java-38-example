package com.ex.collections;

import com.ex.animals.Animal;
import com.ex.animals.Cat;
import com.ex.animals.Cow;
import com.ex.animals.Dog;
import com.sun.source.tree.Tree;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetMain {

    public static void main(String[] args) {

        // Set <- interfata
        // HashSet. TreeSet, LinkedHAshSet <- implementari ale interfetie
        // (similar la List cu ArrayList și LinkedList
        Set<Integer> intHashSet = new HashSet<Integer>();
        Set<Integer> intTreeSet = new TreeSet<Integer>();
        Set<Integer> linkedHashSet = new LinkedHashSet<Integer>();

        intHashSet.add(3);
        intHashSet.add(13);
        intHashSet.add(-3);
        intHashSet.add(3);
        intHashSet.add(1);
        System.out.println("Hash set: ");
        for (Integer nr : intHashSet) {
            System.out.print(nr + " ");
        }

        System.out.println();
        intTreeSet.add(3);
        intTreeSet.add(13);
        intTreeSet.add(-3);
        intTreeSet.add(3);
        intTreeSet.add(1);
        System.out.println("Tree set: ");
        for (Integer nr : intTreeSet) {
            System.out.print(nr + " ");
        }
        System.out.println();


        linkedHashSet.add(3);
        linkedHashSet.add(13);
        linkedHashSet.add(-3);
        linkedHashSet.add(3);
        linkedHashSet.add(1);
        System.out.println("LinkedHash set: ");
        for (Integer nr : linkedHashSet) {
            System.out.print(nr + " ");
        }
        System.out.println();

        Set<Dog> dogs = new TreeSet<>();


        Dog pluto = new Dog("Pluto", 2);
        Dog rex = new Dog("Rex", 1);
        Dog max = new Dog("Max", 3);
        Dog ares = new Dog("Ares", 4);


        dogs.add(pluto);
        dogs.add(rex);
        dogs.add(max);
        dogs.add(pluto);
        dogs.add(ares);

        System.out.println("Dogs:");
        for (Dog d : dogs) {
            System.out.println(d.getId() + " " + d.getName());
        }

        System.out.println(
                "Pluto vs arabella "
                        + pluto.compareTo(new Cat("arabella", 1)));

        Set<Cat> cats = new TreeSet<>();

        Cat tanu = new Cat("tanu", 12);
        Cat motanu = new Cat("motanu", 99);
        Cat mrbigs = new Cat("mrbigs", 43);
        Cat luca = new Cat("luca", 10);

        cats.add(tanu);
        cats.add(motanu);
        cats.add(mrbigs);
        cats.add(luca);

        System.out.println("Cats:");

        for (Cat c : cats) {
            System.out.println(c.getId() + " " + c.getName());
        }

        pluto.compareTo(motanu);

        Set<Animal> animals = new TreeSet<>();

        animals.add(ares);
        animals.add(pluto);
        animals.add(tanu);
        animals.add(motanu);
        animals.add(luca);
        animals.add(new Cow("arabella"));
        animals.add(new Cow("milka"));
        animals.add(new Cow("fulga"));

        System.out.println("animals:");

        for (Animal a : animals) {

            System.out.println(a.getSpecies() + " " + a.getName());
        }
    }
}
