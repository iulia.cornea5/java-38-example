package com.ex.collections;

import java.sql.SQLOutput;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LinkedListMain {

    public static void main(String[] args) {

        List<String> stringLinkedList = new LinkedList<String>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduceti 5 cuvinte: ");

        for (int i = 1; i <= 5; i++) {
            stringLinkedList.add(sc.nextLine());

        }
        int firstWordCounter = 0;
        String firstWord = stringLinkedList.get(0);
        for (int i = 0; i < stringLinkedList.size(); i++) {
            String currentWord = stringLinkedList.get(i);
            if (currentWord.equals(firstWord)) {
                firstWordCounter++;
            }
        }
        System.out.println("Primul cuvant din lista: " + stringLinkedList.get(0) + " , apare de " + firstWordCounter + " ori.");

//        Introduceti 5 cuvinte:
//        doi
//        trei
//        doi
//        doi
//        patru
//        Primul cuvânt: doi, apare de 3 ori.


    }
}
