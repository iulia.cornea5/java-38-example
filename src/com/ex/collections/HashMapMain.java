package com.ex.collections;

import com.ex.animals.Cat;
import com.ex.animals.Dog;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class HashMapMain {

    public static void main(String[] args) {
//        Map<Integer, Dog> dogs = new HashMap<Integer, Dog>();
//        Map<Integer, Dog> dogs = new LinkedHashMap<Integer, Dog>();
        Map<Integer, Dog> dogs = new TreeMap<>();

        Dog pluto = new Dog("Pluto", 91);
        Dog rex = new Dog("Rex", 2342);
        Dog max = new Dog("Max", 3);
        Dog ares = new Dog("Ares", 4234);


        dogs.put(pluto.getId(), pluto);
        dogs.put(2342, rex);
        dogs.put(max.getId(), max);
        dogs.put(ares.getId(), ares);

        System.out.println(dogs.get(91).getName());

//        dogs.keySet();
        for (Integer cheie : dogs.keySet()) {
            System.out.print(cheie + " ");
            System.out.println(dogs.get(cheie).getName());
        }

        for (Dog caine : dogs.values()) {
            System.out.println(caine.getName());
        }


        Dog blacky = new Dog("Blacky", 3);
        dogs.put(blacky.getId(), blacky);
        for (Integer cheie : dogs.keySet()) {
            System.out.print(cheie + " ");
            System.out.println(dogs.get(cheie).getName());
        }

        Integer minKey = Integer.MAX_VALUE;
        Integer maxKey = Integer.MIN_VALUE;

        for (Integer key : dogs.keySet()) {
            if (minKey > key) {
                minKey = key;
            }
            if (maxKey < key) {
                maxKey = key;
            }
        }
        System.out.println("cea mai mica cheie e " + minKey);
        System.out.println("cea mai mare cheie e " + maxKey);


        Integer minLength = Integer.MAX_VALUE;
        Integer maxLength = Integer.MIN_VALUE;

        Dog shortestNameDog = new Dog("", 0);
        Dog longestNameDog= new Dog("", 0);


        for (Dog d : dogs.values()) {
            if (d.getName().length() < minLength) {
                minLength = d.getName().length();
                shortestNameDog = d;
            }
        }
        System.out.println("Shortest name is " +
                shortestNameDog.getName()
                + " with " + minLength + " characters");

        for(Dog d: dogs.values())
        {
            int currentLength = d.getName().length();
            if(currentLength>maxLength){
                maxLength=currentLength;
                longestNameDog=d;
            }
        }
        System.out.println("Longest name is "+ longestNameDog.getName()+" with "+maxLength+" characters");


        Map<String, Dog> dogs2 = new TreeMap<>();

        dogs2.put(pluto.getName(), pluto);
        dogs2.put(ares.getName(), ares);
        dogs2.put(max.getName(), max);

        for (String name: dogs2.keySet()){
            System.out.println(name);
        }

        for (Dog d : dogs2.values()){
            System.out.println(d.getId() + d.getName());
        }

    }
}
