package com.ex.teme.shape;

public class Circle extends Shape {

    private float radius;

    public Circle(float radius) {
        this.radius = radius;
    }

    @Override
    public String getType() {
        return "Circle";
    }

    @Override
    public float getArea() {
        return 3.14f * radius * radius;
    }
}
