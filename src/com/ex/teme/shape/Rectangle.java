package com.ex.teme.shape;

public class Rectangle extends Shape {

    private float length;
    private float width;

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public String getType() {
        return "Rectangle";
    }

    @Override
    public float getArea() {
        return length * width;
    }
}
