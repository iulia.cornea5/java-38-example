package com.ex.teme.shape;

public class Triangle extends Shape {

    private float base;
    private float height;

    public Triangle(float base, float height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public String getType() {
        return "Triangle";
    }

    @Override
    public float getArea() {
        return base * height / 2;
    }
}
