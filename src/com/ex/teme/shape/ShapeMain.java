package com.ex.teme.shape;

public class ShapeMain {

    public static void main(String[] args) {
        Shape[] shapes = new Shape[10];
        shapes[0] = new Triangle(2, 4);
        shapes[1] = new Circle(5);
        shapes[2] = new Rectangle(2, 4);
        shapes[3] = new Triangle(5, 4);
        shapes[4] = new Triangle(12, 4);
        shapes[5] = new Circle(6);
        shapes[6] = new Rectangle(8, 10);
        shapes[7] = new Circle(8);

        // 0 -> 9
        // Index out of Bounds exception pt val index 10
//        for (int i = 0; i <= 10; i++) {
//            System.out.println(shapes[i]);
//        }


        // null.metoda SAU null.variablia SAU null.sdjfsdjk
        // null pointer exception pentru null.printInfo()
//        for (int i = 0; i <= 8; i++) {
//            shapes[i].printInfo();
//        }

        // arthmetic exception
        System.out.println(5/0);
    }
}
