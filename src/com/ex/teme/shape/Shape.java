package com.ex.teme.shape;

public abstract class Shape {

    public abstract String getType();

    public abstract float getArea();

    public void printInfo() {
        System.out.println(getType() + " with an area of " + getArea());
    }


}
