package com.ex.plants;

import com.ex.food.Poisonous;

public class Mushroom implements Poisonous {

    public float getDosePerKg(){
        return 100f;
    }
    public float getDoseForAdult(int kgAdult){
        return getDosePerKg() * kgAdult;
    }

    @Override
    public String getRecipe() {
        return null;
    }


}

