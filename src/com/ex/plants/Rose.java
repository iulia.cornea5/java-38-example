package com.ex.plants;

public class Rose extends Plant {

    public Rose(String name, int aquisitionPrice) {
        this.setName(name);
        this.setAquisitionPrice(aquisitionPrice);
    }

    @Override
    int getSellingPrice() {
        return getAquisitionPrice() * 3;
    }
}
