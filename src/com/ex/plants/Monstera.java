package com.ex.plants;

public class Monstera extends Plant {

    public Monstera(String name, int aquisitionPrice) {
        this.setName(name);
        this.setAquisitionPrice(aquisitionPrice);
    }

    @Override
    int getSellingPrice() {
        return getAquisitionPrice() * 2;
    }
}
