package com.ex.plants;

public abstract class Plant {

    private String family;
    private String name;
    private int aquisitionPrice;
    private boolean isRare;

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAquisitionPrice() {
        return aquisitionPrice;
    }

    public void setAquisitionPrice(int aquisitionPrice) {
        this.aquisitionPrice = aquisitionPrice;
    }

    public boolean isRare() {
        return isRare;
    }

    public void setRare(boolean rare) {
        isRare = rare;
    }

    public String getPlantInfo() {
        return name + " with an aquisition cost of " + aquisitionPrice;
    }

    abstract int getSellingPrice();

//    abstract int getShippingTimeForQuantity(int quantity);
}
