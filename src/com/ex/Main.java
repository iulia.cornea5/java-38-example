package com.ex;

import com.ex.school.Employee;
import com.ex.school.Student;
import com.ex.school.Teacher;
import com.ex.school.enums.Occupation;
import com.ex.shape.Rectangle;
import com.ex.shape.Shape;
import com.ex.shape.Square;
import com.ex.shape.Triangle;

public class Main {

    public static void main(String[] args) {

        Shape[] shapes = new Shape[3];
        Triangle t = new Triangle(3, 52, "pink");
        shapes[0] = t;
        Square s = new Square(7);
        shapes[1] = s;
        Rectangle r = new Rectangle(5, 8, "red");
        shapes[2] = r;

        for (Shape sh : shapes) {
            System.out.println(sh.toString());
        }

    }


}
