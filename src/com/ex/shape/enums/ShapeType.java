package com.ex.shape.enums;

public enum ShapeType {
    TRIANGLE, RECTANGLE, SQUARE, CIRCLE
}
