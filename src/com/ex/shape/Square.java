package com.ex.shape;

import com.ex.shape.enums.ShapeType;

public class Square extends Shape {

    public Square(int base) {
        super(base);
    }

    public Square(int base, String color) {
        super(base, color, ShapeType.SQUARE);
    }

    @Override
    public Integer getArea() {
        return base * base;
    }

    @Override
    public String toString() {
        return "I am a " + color + " square. My base is " + base + " and my area is " + getArea();
    }
}
