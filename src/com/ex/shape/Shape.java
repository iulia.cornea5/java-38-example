package com.ex.shape;


import com.ex.shape.enums.ShapeType;

public abstract class Shape {

    public int base;
    public String color;
    public ShapeType shapeType;

    public Shape(int base) {
        this.base = base;
        this.color = "black";
    }

    public Shape(int base, String color, ShapeType shapeType) {
        this.base = base;
        this.color = color;
        this.shapeType = shapeType;
    }


    public Integer getArea() {
        return null;
    }

    @Override
    public String toString() {
        return "I am a " + color + "shape with base " + base;
    }

    public String tellMeWhatYouAre() {
        return "A general " + color + " shape ";
    }


}
