package com.ex.shape;

import com.ex.shape.enums.ShapeType;

public class Rectangle extends Shape{
    int height;

    public Rectangle(int base, int height) {
        super(base);
        this.height = height;
    }

    public Rectangle(int base, int height, String color) {
        super(base, color, ShapeType.RECTANGLE);
        this.height = height;
    }

    @Override
    public Integer getArea() {
        return base * height;
    }

    @Override
    public String toString() {
        return "I am a " + color + " rectangle. My base is " + base + " and my height is " + height + " and my area is " + getArea();
    }
}
