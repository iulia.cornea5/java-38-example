package com.ex.shape;

import com.ex.shape.enums.ShapeType;

public class Triangle extends Shape {
    public int height;

    public Triangle(int base, int height) {
        super(base);
        this.height = height;
    }

    public Triangle(int base, int height, String color) {
        super(base, color, ShapeType.TRIANGLE);
        this.height = height;
    }

    @Override
    public Integer getArea() {
        return base*height/2;
    }

    @Override
    public String toString() {
        return "I am a " + color + " triangle. My base is " + base + ", my height is " + height + " and my area is " + getArea();
    }

    @Override
    public String tellMeWhatYouAre(){
        return "A " + color + " triangle";
    }
}
