package com.ex.food;

public interface Poisonous {
    float getDosePerKg();

    float getDoseForAdult(int kgAdult);

    String getRecipe();




}
