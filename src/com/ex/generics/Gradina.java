package com.ex.generics;

public interface Gradina<E> {

    // add element to garden
    void addElement(E element);

    E hardestToCareFor();

    String hardestToCareForName();
}
