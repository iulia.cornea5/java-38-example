package com.ex.generics.plante;

public class Pom {

    public int dificultate;
    public String nume;

    public Pom(int dificultate, String nume) {
        this.dificultate = dificultate;
        this.nume = nume;
    }
}
