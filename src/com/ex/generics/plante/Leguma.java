package com.ex.generics.plante;

public class Leguma {
    private String denumire;
    private int dificultateIngrijire;

    public Leguma(String denumire, int dificultateIngrijire) {
        this.denumire = denumire;
        this.dificultateIngrijire = dificultateIngrijire;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public int getDificultateIngrijire() {
        return dificultateIngrijire;
    }

    public void setDificultateIngrijire(int dificultateIngrijire) {
        this.dificultateIngrijire = dificultateIngrijire;
    }
}
