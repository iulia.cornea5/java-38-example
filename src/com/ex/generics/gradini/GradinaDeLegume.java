package com.ex.generics.gradini;

import com.ex.generics.Gradina;
import com.ex.generics.plante.Leguma;

import java.util.ArrayList;
import java.util.List;

public class GradinaDeLegume implements Gradina<Leguma> {

    public List<Leguma> legume = new ArrayList<>();

    @Override
    public void addElement(Leguma element) {
        legume.add(element);
    }

    @Override
    public Leguma hardestToCareFor() {
        if(legume.isEmpty()) {
            return null;
        }
        Leguma ceaMaiDificila = legume.get(0);
        for(Leguma l : legume) {
            if(l.getDificultateIngrijire() > ceaMaiDificila.getDificultateIngrijire()) {
                ceaMaiDificila = l;
            }
        }
        return ceaMaiDificila;
    }

    @Override
    public String hardestToCareForName() {
        return hardestToCareFor().getDenumire();
    }
}
