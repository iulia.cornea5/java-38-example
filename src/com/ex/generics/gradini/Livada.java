package com.ex.generics.gradini;

import com.ex.generics.Gradina;
import com.ex.generics.plante.Pom;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Livada implements Gradina<Pom> {

    public Map<Integer,Pom> pomi = new TreeMap();
    private Pom hardestToCareFor;

    @Override
    public void addElement(Pom element) {
        pomi.put(element.dificultate,element);
        if(hardestToCareFor == null)
            hardestToCareFor = element;
        if(element.dificultate > hardestToCareFor.dificultate)
            hardestToCareFor = element;
    }

    @Override
    public Pom hardestToCareFor() {
        return hardestToCareFor;

    }

    public String hardestToCareForName(){
        return hardestToCareFor().nume;
    }
}
