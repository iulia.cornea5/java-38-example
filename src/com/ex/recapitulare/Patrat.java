package com.ex.recapitulare;

import java.util.Scanner;

public class Patrat extends Forma {
    private double latura;

    public Patrat(){

    }

    public Patrat(double latura) {
        this.latura = latura;
    }

    public void initFromKeyboard(Scanner sc) {
        System.out.println("Dati latura patratului: ");
        this.latura = sc.nextDouble();
        sc.nextLine();
    }

    @Override
    public double getArea() {
        return latura * latura;
    }
}
