package com.ex.recapitulare;

public abstract class Forma implements Comparable<Forma> {




    @Override
    public int compareTo(Forma form) {
        // cerc < triunghi < patrat

        if(this.getClass().equals(form.getClass())) {
            if (this.getArea() > form.getArea()) {
                return 1;
            } else if (this.getArea() < form.getArea()) {
                return -1;
            } else {
                return 0;
            }
        } else {
            if (this.getClass().equals(Cerc.class)) {
                return -1;
            }
            if (this.getClass().equals(Patrat.class)) {
                return 1;
            }
            if (form.getClass().equals(Cerc.class)) {
                return 1;
            }
            if (form.getClass().equals(Patrat.class)) {
                return -1;
            }
        }
        return 0;
    }

    public abstract double getArea();
}
