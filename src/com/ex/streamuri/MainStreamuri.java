package com.ex.streamuri;

import com.ex.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainStreamuri {
    public static void main(String[] args) {

        int varInt;
        Integer varInteger;

        List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 20);

        // lambda expression
        List<Integer> evenIntegers = integerList.stream().filter((i) -> {
            // return (i%2 == 0);
            // return i%2 == 0 ? true : false;
            if (i % 2 == 0)
                return true;
            else
                return false;
        }).collect(Collectors.toList());

        for (Integer i : evenIntegers) {
            System.out.println(i);
        }

        evenIntegers.forEach((i) -> {
            System.out.println(i);
        });

        evenIntegers.forEach(i -> System.out.println(i));

        List<Integer> imparIntegers = integerList.stream().filter(i -> {
            return i % 2 == 1;
        }).collect(Collectors.toList());

        List<Integer> imparIntegers2 = integerList
                .stream()
                .filter(i -> i % 2 == 1 )
                .collect(Collectors.toList());
        imparIntegers2
                .forEach(System.out::println);

        imparIntegers2
                .forEach(MainStreamuri::afiseaza);

        integerList
                .stream()
                .filter(i -> i % 2 == 1 )
                .forEach(imparInt -> System.out.println(imparInt));













        List<Integer> ints = new ArrayList<>();
        for(int i = 1; i<= 1000; i++) {
            ints.add(i);
        }
        System.out.println("------");
//        ints.stream().forEach(System.out::println);
//        ints.parallelStream().forEach(System.out::println);

        ints.parallelStream().forEachOrdered(System.out::println);



    }

    public static void afiseaza(Object o) {
        System.out.print(o.toString() + " - ");
    }
}
