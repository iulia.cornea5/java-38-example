package com.ex.streamuri;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExercitiiStreamuriMain {

    public static void main(String[] args) {


        // --------------------------------------- .map ----------------------------------------------

        // imaginea in oglinda a string-urilor -> abc -> cba
        List<String> stringL = Arrays.asList("abc", "def", "ghijk", "mama");

        stringL.stream()
                .map(s -> {
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.println(s));

        stringL.stream().map(s -> s.toUpperCase()).forEach(System.out::println);

        stringL.stream().map(s -> s.replace('a', '*')).forEach(s -> System.out.println(s));
        stringL.stream().map(s -> s.replace('a', '*')).forEach(System.out::println);

        // filter length<=3     >>>>>   map replace 'b' cu '7'\

        stringL.stream()
                .filter(s -> s.length() <= 3)
                .map(s -> s.replace('b', '7'))
                .forEach(s -> System.out.println(s));

        // 2,4,6,654,2,4,6,7,8,3,2,12,65,645,55,3423,23
        // dublul numerelor pare

        List<Integer> integerList = Arrays.asList(2, 4, 6, 654, 2, 4, 6, 7, 8, 3, 2, 12, 65, 645, 55, 3423, 23);

        integerList.stream()
                .filter(integer -> {
                    if (integer % 2 == 0) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .map(integer -> integer*2)
                .forEach(integer -> System.out.println(integer));


        // --------------------------------------- .filter ----------------------------------------------

        // avem o lista de nr si vrem sa le pastram doar pe cele mai mari decât 10

        integerList = Arrays.asList(2, 5, 8, 7, 9, 17, 37, 47, 23, 25, 29);


        List<Integer> filtered;
        filtered = integerList.stream().filter(t -> {
            if (t > 10)
                return true;
            else
                return false;
        }).collect(Collectors.toList());

        filtered = integerList.stream().filter(t -> t > 10).collect(Collectors.toList());


        // ultima cifră nr%10 <- restul împărțirii la 10
        // ex 39%10 -> 9;       256%10 -> 6     130%10 -> 0
        filtered = integerList.stream().filter(t -> t % 10 == 7).collect(Collectors.toList());

        filtered.forEach(t -> System.out.println(t));


        List<String> stringList = Arrays.asList("abc", "abcde", "abcdef", "bcd", "def");

        List<String> filteredStrings;
        filteredStrings = stringList.stream().filter(s -> {
            if (s.length() <= 3)
                return true;
            else
                return false;
        }).collect(Collectors.toList());

        filteredStrings.forEach(s -> System.out.println(s));

        // Totul scris intr-o singura linie
        stringList.stream().filter(s -> s.length() <= 3).forEach(s -> System.out.print(s + " "));
        stringList.stream().filter(s -> s.length() <= 3).forEach(System.out::println);


        int a = 4, v = 2;
        int c = a + v;
        System.out.println("Suma = " + c);

        System.out.println("Suma= " + (a + v));


    }

}

