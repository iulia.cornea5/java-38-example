package com.ex.communications;

public interface Receiver {

    String getReceptionChannel();

    String receiveMessage();
}
