package com.ex.communications.impl;

import com.ex.communications.Receiver;

import java.util.Scanner;

public class ConsoleReceiver implements Receiver {
    @Override
    public String getReceptionChannel(){
        return "Console";
    }
    public String receiveMessage(){
        Scanner scn = new Scanner(System.in);
       String message = scn.nextLine();
       return message;
    }


}
