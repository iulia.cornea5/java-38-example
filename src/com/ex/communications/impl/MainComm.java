package com.ex.communications.impl;

import com.ex.communications.Sender;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class MainComm {
    public static void main(String[] args)  {

//        Sender sender1 = new ConsoleSender();
        Sender sender1 = new FileSender();


        Sender sender;

        Scanner keyboard = new Scanner(System.in);
        String senderType = keyboard.nextLine();
        if(senderType.equals("console")) {
            sender = new ConsoleSender();
        } else {
            sender = new FileSender();
        }

        System.out.println("------------- try sending a message to " + sender1.getDestinationChannel() + "---------------------");
        boolean messageWasSent = sender1.sendMessage("Hello you have reached Orange, " +
                "for English press 1, for French press 2, for Spanish press 3");

        if (messageWasSent) {
            System.out.println("------------- message was sent successfully ---------------------");

        } else {
            System.out.println("------------- message was NOT sent ---------------------");
        }


    }
}
