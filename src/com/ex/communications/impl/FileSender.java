package com.ex.communications.impl;

import com.ex.communications.Sender;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class FileSender implements Sender {

    private final String FILE = "out.txt";

    @Override
    public String getDestinationChannel() {
//        FILE = "nu pot asigna alta valoare lui FILE";
        return "File " + FILE;
    }

    @Override
    public boolean sendMessage(String message) {

        if(!isOpen() || message.length() > getMaximLength()) {
            return false;
        }

        // write to file -> char 'v'
        // buffered writer -> String "viorica" -> 'v' + 'i' + ....'a'
        try {
            BufferedWriter toFile = new BufferedWriter( new FileWriter("out.txt"));
            toFile.write(message);
            toFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public int getMaximLength() {
        return 100;
    }

    @Override
    public boolean isOpen() {
        return true;
    }
}
