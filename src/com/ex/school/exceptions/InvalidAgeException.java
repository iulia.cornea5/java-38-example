package com.ex.school.exceptions;

// unchecked exception
public class InvalidAgeException extends RuntimeException{
    public InvalidAgeException(int wrongValue) {
        super("Invalid age for value" + wrongValue+ " It should be positive number ");

    }
}
