package com.ex.school.exceptions;

// checked exception
// <-
public class InvalidCnpException extends RuntimeException {

    public InvalidCnpException(String wrongValue) {
        super("Invalid cnp for value " + wrongValue + ". It should have 13 characters" +
                " but it has " + wrongValue.length());
    }
}
