package com.ex.school;

import com.ex.school.enums.Occupation;
import com.ex.school.exceptions.InvalidCnpException;

// extends răspunde/e analog la "is a" ex: Teacher is a Person
public class Employee extends Person {

    private Occupation occupation;

    public Employee(String cnp, String firstName, String lastName, int age, Occupation occupation) throws InvalidCnpException {
        // super apelează constructorul SUPERclasei Person
        super(cnp, firstName, lastName, age);
        this.occupation = occupation;
    }

    public Employee(String cnp, Occupation occupation) {
        // super apelează constructorul SUPERclasei Person
        super(cnp);
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return super.toString() + " si ocupatia de " + occupation;
    }
}

