package com.ex.school;

import com.ex.school.exceptions.InvalidAgeException;
import com.ex.school.exceptions.InvalidCnpException;

public class Person {

    private String cnp;

    private String firstName;

    private String lastName;

    private Integer age;


    public Person (String cnp, String firstName, String lastName, Integer age) throws InvalidAgeException  {
        // daca cnp nu e de 13 caractere, arunca o exceptie
        if (cnp.length() != 13) {
            InvalidCnpException e = new InvalidCnpException(cnp);;
            throw e;
        }
        if(age < 0){
            InvalidAgeException a = new InvalidAgeException(age);
            throw a;
        }
        this.cnp = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Person (String cnp) {
        this.cnp = cnp;
    }


    public String getCnp() {
        return cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName(){
        return  this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String toString() {
        return "Mă numesc " + firstName + " " + lastName +
                " și am " + age + " ani.";
    }
}
